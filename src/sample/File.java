package sample;

/**
 * @author ：刘天予
 * @date ：Created in 2020/5/15 16:13
 * @description：实体
 * @modified By：
 * @version: $
 */
public class File {

    private String fileName;

    private String filePath;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
